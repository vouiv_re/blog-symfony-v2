CREATE DATABASE  IF NOT EXISTS `db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `db_admin`
--

DROP TABLE IF EXISTS `db_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudonym` varchar(45) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_admin`
--

LOCK TABLES `db_admin` WRITE;
/*!40000 ALTER TABLE `db_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_article`
--

DROP TABLE IF EXISTS `db_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `lead_paragraph` text,
  `author` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_article`
--

LOCK TABLES `db_article` WRITE;
/*!40000 ALTER TABLE `db_article` DISABLE KEYS */;
INSERT INTO `db_article` VALUES (1,'My first Article','You have nothing, nothing to threaten me with. Nothing to do with all your strength.',NULL,NULL,NULL,NULL,'But I know the rage that drives you. That impossible anger strangIing the grief until the memory of your loved one is just poison in your veins. And one day, you catch yourself wishing the person you loved had never existed so you\'d be spared your pain.'),(5,'Patat pouit pouit','Just know that there are those of us who care about what you do with your future.',NULL,NULL,NULL,NULL,'Do you wanna know why I use a knife? Guns are too quick. You can\'t savor all the... little emotions. You see, in their last moments, people show you who they really are. So in a way, I knew your friends better than you ever did. Would you like to know which of them were cowards?\r\n\r\nYou know how to fight six men. We can teach you how to engage 600.\r\n\r\nLet me get this straight. You think that your client, one of the wealthiest, most powerful men in the world is secretly a vigilante who spends his nights beating criminals to a pulp with his bare hands and your plan is to blackmail this person? Good luck.'),(6,'Batman Ipsum','Every man who has lotted here over the centuries, has looked up to the light and imagined climbing to freedom. So easy, so simple! And like shipwrecked men turning to seawater foregoing uncontrollable thirst, many have died trying.',NULL,NULL,NULL,NULL,'Every year, I took a holiday. I went to Florence, this cafe on the banks of the Arno. Every fine evening, I would sit there and order a Fernet Branca. I had this fantasy, that I would look across the tables and I would see you there with a wife maybe a couple of kids. You wouldn\'t say anything to me, nor me to you. But we would both know that you\'ve made it, that you were happy. I never wanted you to come back to Gotham. I always knew there was nothing here for you except pain and tragedy and I wanted something more for you than that. I still do.'),(7,'Batman Ipsum 1','You wanna know how I got them? So I had a wife. She was beautiful, like you, who tells me I worry too much, who tells me I ought to smile more, who gambles and gets in deep with the sharks. Hey. One day they carve her face. And we have no money for surgeries. She can\'t take it. I just wanna see her smile again. I just want her to know that I don\'t care about the scars. So, I do this to myself. And you know what? She can\'t stand the sight of me. She leaves. Now I see the funny side. Now I\'m always smiling.',NULL,NULL,NULL,NULL,'My name is Merely Ducard but I speak for Ra\'s al Ghul... a man greatly feared by the criminal underworld. A mon who can offer you a path. Someone like you is only here by choice. You have been exploring the criminal fraternity but whatever your original intentions you have to become truly lost. The path of a man who shares his hatred of evil and wishes to serve true justice. The path of the League of Shadows.'),(8,'Batman Ipsum 2','You wanna know how I got them? So I had a wife. She was beautiful, like you, who tells me I worry too much, who tells me I ought to smile more, who gambles and gets in deep with the sharks. Hey. One day they carve her face. And we have no money for surgeries. She can\'t take it. I just wanna see her smile again. I just want her to know that I don\'t care about the scars. So, I do this to myself. And you know what? She can\'t stand the sight of me. She leaves. Now I see the funny side. Now I\'m always smiling.',NULL,NULL,NULL,NULL,'You wanna know how I got them? So I had a wife. She was beautiful, like you, who tells me I worry too much, who tells me I ought to smile more, who gambles and gets in deep with the sharks. Hey. One day they carve her face. And we have no money for surgeries. She can\'t take it. I just wanna see her smile again. I just want her to know that I don\'t care about the scars. So, I do this to myself. And you know what? She can\'t stand the sight of me. She leaves. Now I see the funny side. Now I\'m always smiling.');
/*!40000 ALTER TABLE `db_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_article_category`
--

DROP TABLE IF EXISTS `db_article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_article_category` (
  `id_article` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  PRIMARY KEY (`id_article`,`id_category`),
  KEY `fk_db_article_category_2_idx` (`id_category`),
  CONSTRAINT `fk_db_article_category_1` FOREIGN KEY (`id_article`) REFERENCES `db_article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_db_article_category_2` FOREIGN KEY (`id_category`) REFERENCES `db_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_article_category`
--

LOCK TABLES `db_article_category` WRITE;
/*!40000 ALTER TABLE `db_article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_article_tag`
--

DROP TABLE IF EXISTS `db_article_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_article_tag` (
  `id_article` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  PRIMARY KEY (`id_article`,`id_tag`),
  KEY `fk_db_article_tag_2_idx` (`id_tag`),
  CONSTRAINT `fk_db_article_tag_1` FOREIGN KEY (`id_article`) REFERENCES `db_article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_db_article_tag_2` FOREIGN KEY (`id_tag`) REFERENCES `db_tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_article_tag`
--

LOCK TABLES `db_article_tag` WRITE;
/*!40000 ALTER TABLE `db_article_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_article_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_category`
--

DROP TABLE IF EXISTS `db_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_category`
--

LOCK TABLES `db_category` WRITE;
/*!40000 ALTER TABLE `db_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26  9:15:49
