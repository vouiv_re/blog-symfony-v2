<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ArticleRepository $repo)
    {
        $result = $repo->getAll();
        return $this->render('admin/index.html.twig', [
            'result'  => $result,
            'subtitle' => "Admin"
        ]);
    }
}
