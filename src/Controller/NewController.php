<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;

class NewController extends Controller
{
    /**
     * @Route("/admin/new", name="new")
     */
    public function index(Request $request, ArticleRepository $repo)
    {
      $form = $this->createForm(ArticleType::class);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $repo->add($form->getData());
          return $this->redirectToRoute("admin");
      }

      return $this->render('admin/edition.html.twig', [
          "form" => $form->createView(),
          'subtitle' => "New"
      ]);
    }
}