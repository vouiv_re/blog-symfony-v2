<?php

namespace App\Repository;

use App\Entity\Article;

class ArticleRepository
{

    public function getAll() : array
    {
        $articles = [];
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("SELECT * FROM db_article ORDER BY id DESC");

            $query->execute();

            foreach ($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }

    public function add(Article $article)
    {
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("INSERT INTO db_article (title, lead_paragraph, content) VALUES (:title, :lead_paragraph, :content)");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":lead_paragraph", $article->lead_paragraph);
            $query->bindValue(":content", $article->content);

            $query->execute();

            $article->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
    }

    public function update(Article $article)
    {
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("UPDATE db_article SET title=:title, lead_paragraph=:lead_paragraph, content=:content WHERE id=:id");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":lead_paragraph", $article->lead_paragraph);
            $query->bindValue(":content", $article->content);
            $query->bindValue(":id", $article->id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id)
    {
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("DELETE FROM db_article WHERE id=:id");

            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function getById(int $id): ?Article
    {
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("SELECT * FROM db_article WHERE id=:id");
            
            $query->bindValue(":id", $id);

            $query->execute();

            $result = $query->fetchAll();

            if (count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }

}